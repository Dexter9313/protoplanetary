#version 150 core

in float xposition;

uniform float start;
uniform float scale;

void main()
{
	gl_Position = vec4(start + scale*xposition, 0.1, 0.0, 1.0);
}
