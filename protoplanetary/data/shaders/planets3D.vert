#version 150 core

in vec3 position;
in float status;
in vec3 position2;
in float status2;

uniform mat4 camera;

uniform float maxR = 1.0;
uniform float timestep = 0.0;

out float f_status;
out float f_status2;

void main()
{
	vec3 p = mix(position, position2, vec3(fract(timestep)));
	vec4 pos = camera * vec4(30.0 * p / maxR, 1.0);

    gl_Position = pos;

	f_status = status;
	f_status2 = status2;
}
