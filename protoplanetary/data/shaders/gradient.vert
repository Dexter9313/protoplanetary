#version 150 core

in vec2 position;

out vec2 f_pos;

void main()
{
	f_pos = position;
	f_pos.x *= 0.05;
	f_pos.x += -0.95;
	gl_Position = vec4(f_pos, 0.0, 1.0);
}
