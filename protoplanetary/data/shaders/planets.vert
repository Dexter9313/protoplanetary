#version 150 core

in vec3 position;
in float status;
in vec3 position2;
in float status2;

uniform mat4 camera;

uniform float zoom = 1.0;
uniform float aspectRatio = 1.0;
uniform float timestep = 0.0;

void main()
{
	vec3 p = mix(position, position2, vec3(fract(timestep)));

	vec2 pos = p.xy * zoom;
	if(status != 0.0 || status2 != 0.0)
	{
		pos = vec2(-10000.0);
	}

	pos.y *= aspectRatio;

	// pos = gl_VertexID * vec2(1.0) / 100.0;
    gl_Position = vec4(pos, 0.0, 1.0);
}
