#version 150 core

in vec3 f_pos;

uniform vec3 campos;

uniform float time;
uniform vec3 starColor;
uniform float samples = 500.0;

uniform sampler3D preproc;

out vec4 outColor;

#include <commons3D.glsl>

vec3 getDirectStarLight(vec3 p)
{
	vec3 coord = vec3(length(p.xy), (p.z*5.0 + 0.5), time);
	return texture(preproc, coord).rgb;
}

float phase(float mu)
{
	float g = 0.5;
	return (1.0-g*g)/pow(1.0+g*g-2.0*g*mu, 3.0/2.0) / (4.0*3.1415);
}

vec3 accumulateLight(vec3 pA, vec3 pB)
{
	int SAMPLES = int(length(pB - pA) * samples);
	float l = length(pB-pA);
	vec3 ds           = (pB - pA) / float(SAMPLES);
	vec3 v            = pA + ds * 0.5;

	vec3 integral = vec3(0.0);
	float densIntegral = 0.0;
	for(int i = 0; i < SAMPLES; i++)
	{
		densIntegral += getDensity(vec4(v, time));
		integral += getDirectStarLight(v) * phase(dot(normalize(v), normalize(ds))) * pow(10.0, -1.0 * densIntegral / 2.5);
		v += ds;
	}
	return 1.0e10 * starColor * integral * length(ds);
}

void main()
{
	outColor.rgb = accumulateLight(campos, f_pos);
}
