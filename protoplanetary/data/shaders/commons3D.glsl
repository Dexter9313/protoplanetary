
uniform sampler2D tex;
uniform sampler2D tex2;

uniform float minR;
uniform float maxR;
uniform float pshUnit = 6.68459e-14; // in AU
uniform float autogravitation = 1.0;

float getDensity(vec4 pt)
{
	vec2 c = pt.xy / 30.0;
	float R = length(c) * maxR;

	float logR = log(R);
	float minLogR = log(minR);
	float maxLogR = log(maxR);

	vec2 coord;
	coord.x = (logR - minLogR) / (maxLogR - minLogR);
	coord.y = pt.w;

	vec4 result = texture(tex, coord);
	vec4 result2 = texture(tex2, coord);
	float density = result.r;
	float scaleHeight = result.b;
	float H0 = result2.g;
	float H1 = result2.b;
	if(scaleHeight == 0.0) // prevent div by 0
	{
		scaleHeight = 1.0e-7;
	}
	if(H0 == 0.0)
	{
		H0 = 1.0e-7;
	}
	if(H1 == 0.0)
	{
		H1 = 1.0e-7;
	}
	scaleHeight *= pshUnit;
	scaleHeight *= 30.0 / maxR; // from AU to cylinder coordinates
	H0 *= pshUnit;
	H0 *= 30.0 / maxR; // from AU to cylinder coordinates
	H1 *= pshUnit;
	H1 *= 30.0 / maxR; // from AU to cylinder coordinates
	if(autogravitation == 0.0)
	{
		return density * exp(-0.5*pow(abs(pt.z) / scaleHeight, 2));
	}
	return density * exp(-(abs(pt.z)/H0 + pow(pt.z / H1, 2.0)));
}

float getDensityIntegral(vec4 ptA, vec4 ptB)
{
	const int SAMPLES = 1000;
	float l = length(ptB-ptA);
	vec4 ds           = (ptB - ptA) / float(SAMPLES);
	vec4 v            = ptA + ds * 0.5;

	float integral = 0.0;
	for(int i = 0; i < SAMPLES; i++)
	{
		integral += getDensity(v);
		v += ds;
	}
	return integral * length(ds);
}

