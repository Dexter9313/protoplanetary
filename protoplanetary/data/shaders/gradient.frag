#version 150 core

in vec2 f_pos;
uniform float alpha = 1.0;
uniform float exposure = 1.0;
uniform float dynamicrange = 1.0;
out vec4 outColor;

#include <gradient/gradient.glsl>
#include <inv_exposure.glsl>

void main()
{
	float t = f_pos.y + 0.5;
	outColor.rgb = evaluateGradient(limits.x + t * (limits.y - limits.x));
	outColor.a = alpha;
	outColor.rgb = inv_exposure(outColor.rgb, dynamicrange, exposure);
}
