#version 150 core

in vec2 texCoord;

uniform sampler2D tex;
uniform sampler2D texIntegral;
uniform float minR;
uniform float maxR;

// PBR = 0,
// DENSITY = 1,
// TEMPERATURE = 2,
// PRESSURE_SCALE = 3,
// TOOMRE = 4,
uniform int ul;
uniform int ur;
uniform int dl;
uniform int dr;

uniform vec3 starColor;
uniform float pshUnit = 6.68459e-14; // in AU
uniform float time;
uniform float zoom = 1.0;
uniform float aspectRatio = 1.0;
uniform float exposure = 1.0;
uniform float dynamicrange = 1.0;

out vec4 outColor;

#include <gradient/gradient.glsl>
#include <inv_exposure.glsl>

vec3 render(int type, vec4 data, float densityIntegral, float R)
{
	vec3 result;
	if(type == 0)
	{
		float density = data.r;
		result = vec3(4.3, 5.3, 8.5) * densityIntegral;
		result = pow(vec3(10.0), -1.0 * result / 2.5);
		result *= density / (R*R);
		result *= 1.0e10 * starColor;
	}
	else if(type == 1)
	{
		float density = data.r;
		result = evaluateGradient(density);
		result = inv_exposure(result, dynamicrange, exposure);
	}
	else if(type == 2)
	{
		float temp = data.g;
		result = evaluateGradient(temp);
		result = inv_exposure(result, dynamicrange, exposure);
	}
	else if(type == 3)
	{
		float psh = data.b;
		psh *= pshUnit;
		result = evaluateGradient(psh);
		result = inv_exposure(result, dynamicrange, exposure);
	}
	else if(type == 4)
	{
		float toomre = data.a;
		result = evaluateGradient(toomre);
		result = inv_exposure(result, dynamicrange, exposure);
	}
	return result;
}

void main()
{
	vec2 c = texCoord;
	c.y /= aspectRatio;
	if(length(c) < 0.002)
	{
		outColor = vec4(starColor, 1.0);
		outColor.rgb = inv_exposure(outColor.rgb, dynamicrange, exposure);
		return;
	}
	float R = length(c) * maxR;
	R /= zoom;

	if(R > maxR || R < minR)
		discard;

	float logR = log(R);
	float minLogR = log(minR);
	float maxLogR = log(maxR);

	vec2 coord;
	coord.x = (logR - minLogR) / (maxLogR - minLogR);
	coord.y = time;

	vec4 data = texture(tex, coord);
	float densityIntegral = texture(texIntegral, coord).r;

	if(texCoord.y > 0.0)
	{
		if(texCoord.x < 0.0)
		{
			outColor.rgb = render(ul, data, densityIntegral, R);
		}
		else
		{
			outColor.rgb = render(ur, data, densityIntegral, R);
		}
	}
	else
	{
		if(texCoord.x < 0.0)
		{
			outColor.rgb = render(dl, data, densityIntegral, R);
		}
		else
		{
			outColor.rgb = render(dr, data, densityIntegral, R);
		}
	}
}
