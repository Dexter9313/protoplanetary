#version 150 core

in float f_status;
in float f_status2;
uniform vec3 color;
uniform float alpha = 1.0;
uniform float exposure = 1.0;
uniform float dynamicrange = 1.0;
out vec4 outColor;

#include <inv_exposure.glsl>

void main()
{
	float d = distance(gl_PointCoord, vec2(0.5, 0.5));
	if(f_status != 0.0 || f_status2 != 0.0 || d > 0.5)
	{
		discard;
	}
	outColor = vec4(color, alpha);
	outColor.rgb = inv_exposure(outColor.rgb, dynamicrange, exposure);
}
