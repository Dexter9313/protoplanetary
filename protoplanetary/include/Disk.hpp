/*
    Copyright (C) 2022 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DISK_HPP
#define DISK_HPP

#include <cfloat>
#include <vector>

#include <QElapsedTimer>

#include "ControlPanel.hpp"
#include "ShaderProgram.hpp"
#include "ToneMappingModel.hpp"
#include "gl/GLHandler.hpp"

class QString;

class Disk
{
  public:
	enum class Variable
	{
		SIGMA = 0, // g/cm²
		TEMPE = 1, // K
		P_S_H = 2, // AU
		QTOOM = 3,
	};

	Disk(QString const& diskdirpath, QString const& planetdirpath);
	float getMinTime() const { return minTime; };
	float getMaxTime() const { return maxTime; };
	float getMinR() const { return minR; };
	float getMaxR() const { return maxR; };
	float getTimeYears()
	{
		return minTime
		       + (maxTime - minTime) * controlPanel.getCorrectedAnimationTime();
	};
	float getAnimationTime() { return controlPanel.getAnimationTime(); };
	float getCorrectedAnimationTime()
	{
		return controlPanel.getCorrectedAnimationTime();
	};
	ControlPanel& getControlPanel() { return controlPanel; };
	float getData(float relTime, float relScreenSpace, float zoom,
	              Variable var);
	void reloadPreproc();
	void render(float zoom, float aspectRatio, ToneMappingModel const& tmm);
	void render3D(QVector3D const& campos, ToneMappingModel const& tmm);
	void renderGui(QSize const& targetSize);

  private:
	const unsigned int variablesNumber = 4;
	unsigned int spacesteps            = 0;
	unsigned int timesteps             = 0;
	unsigned int planets               = 0;

	float minR = FLT_MAX;
	float maxR = FLT_MIN;

	float minSig = FLT_MAX;
	float maxSig = FLT_MIN;

	float minT = FLT_MAX;
	float maxT = FLT_MIN;

	float minPsh = FLT_MAX;
	float maxPsh = FLT_MIN;

	const float pshUnit = QSettings().value("data/pshUnit").toString() == "au"
	                          ? 1.f
	                          : 6.68459e-14;

	void loadPlanets(QString const& dirpath);
	void loadDisk(QString const& dirpath);

	std::vector<float> diskData;
	std::vector<float> diskData2; // contains rho_0 H_0 and H_1
	std::vector<float> diskDataDensityIntegral;
	std::vector<float> diskDataDensityIntegral3D;
	float getData(unsigned int timestep, unsigned int spacestep,
	              Variable var) const;
	void setData(unsigned int timestep, unsigned int spacestep, Variable var,
	             float val);

	std::vector<float> planetData;

	// for progress bar
	unsigned int timestep = 0;

	// gpu assets
	// disk
	ShaderProgram shader;
	GLMesh mesh = {};
	ShaderProgram shader3D;
	ShaderProgram preproc3D;
	GLMesh mesh3D = {};
	std::unique_ptr<GLTexture> tex;
	std::unique_ptr<GLTexture> tex2;
	std::unique_ptr<GLTexture> texIntegral;
	std::unique_ptr<GLTexture> texIntegral3D;
	// planets
	GLShaderProgram shaderPlanets;
	GLShaderProgram shaderPlanets3D;
	GLMesh meshPlanets = {};
	bool autograv      = false;
	// scale
	float closestGreatUnit = 0.f;
	GLShaderProgram shaderScale;
	GLMesh meshScale;
	// gradient display
	ShaderProgram gradientDispShader;
	GLMesh gradientDispMesh;

	// animation
	float minTime = FLT_MAX;
	float maxTime = FLT_MIN;

	grd::Gradient gradient;
	ControlPanel controlPanel;
};

#endif // DISK_HPP
