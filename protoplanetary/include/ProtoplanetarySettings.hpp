/*
    Copyright (C) 2022 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PROTOPLANETARYSETTINGS_HPP
#define PROTOPLANETARYSETTINGS_HPP

#include "SettingsWidget.hpp"

class ProtoplanetarySettings : public SettingsWidget
{
  public:
	ProtoplanetarySettings(QWidget* parent)
	    : SettingsWidget(parent)
	{
		this->insertGroup("data", tr("Data"), 0);
		this->addDirPathSetting("diskdir", {}, tr("Disk dir"));
		this->addDirPathSetting("planetdir", {}, tr("Planet dir"));
		this->addBoolSetting("cpupreproc3D", false,
		                     tr("Use CPU to preproc vol. rendering"));
		this->addStringAmongListSetting("pshUnit", {"au", "cm"}, {"AU", "cm"},
		                                "Pressure Scale Height unit : ");
	};
};

#endif // PROTOPLANETARYSETTINGS_HPP
