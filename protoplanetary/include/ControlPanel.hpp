/*
    Copyright (C) 2023 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CONTROLPANEL_HPP
#define CONTROLPANEL_HPP

#include "gradient/GradientSelector.hpp"
#include "gui/ColorSelector.hpp"
#include "gui/FontSelector.hpp"

#include <QElapsedTimer>
#include <QTextEdit>

class ControlPanel : public QWidget
{
  public:
	enum class VisType
	{
		PBR            = 0,
		DENSITY        = 1,
		TEMPERATURE    = 2,
		PRESSURE_SCALE = 3,
		TOOMRE         = 4,
	};
	ControlPanel(grd::Gradient& gradient);
	float getAnimationTime();
	float getCorrectedAnimationTime();
	VisType getVisTypeUL() const;
	VisType getVisTypeUR() const;
	VisType getVisTypeDL() const;
	VisType getVisTypeDR() const;
	QString getVisTypeStr(VisType visType) const;
	bool is3D() const { return is3DCbox.isChecked(); };
	bool autoGravitation() const { return autogravCbox.isChecked(); };
	unsigned int get3DSamples() const { return samples.value(); };
	bool drawMouseHoverTooltip() const
	{
		return mouseHoverTooltip.isChecked();
	};
	bool drawYear() const { return drawYearCbox.isChecked(); };
	bool drawScale() const { return drawScaleCbox.isChecked(); };
	bool drawColorMap() const { return drawColorMapCbox.isChecked(); };
	bool drawGui() const { return drawGuiCbox.isChecked(); };
	QColor getGuiColor() const { return guiColor.getCurrentColor(); };
	QFont getGuiFont() const { return guiFont.getCurrentFont(); };
	int getStarTemperature() const { return starTemp.value(); };
	QColor getPlanetsColor() const { return planetsColor.getCurrentColor(); };
	QString getCustomText() const { return customText.toPlainText(); };

	void setTimesteps(unsigned int timesteps)
	{
		minTimestep.setMaximum(timesteps - 1);
		maxTimestep.setMaximum(timesteps - 1);
		maxTimestep.setValue(timesteps - 1);
	};

	bool updateTimer(bool videoModeOn);

  private:
	QSlider animTimeSlider;
	QSpinBox minTimestep;
	QSpinBox maxTimestep;
	QSpinBox animDuration;
	QPushButton playPauseBtn;
	QPushButton renderVidBtn;
	QCheckBox quitWhenRendered;
	QCheckBox is3DCbox;
	QCheckBox autogravCbox;
	QSpinBox samples;
	QCheckBox mouseHoverTooltip;
	QComboBox ulCbox;
	QComboBox urCbox;
	QComboBox dlCbox;
	QComboBox drCbox;
	QCheckBox drawYearCbox;
	QCheckBox drawScaleCbox;
	QCheckBox drawColorMapCbox;
	QCheckBox drawGuiCbox;
	ColorSelector guiColor;
	FontSelector guiFont;
	QSpinBox starTemp;
	ColorSelector planetsColor;
	QTextEdit customText;
	GradientSelector selector;

	QStringList visTypesLabels;

	float pausedAt = 0.f;
	QElapsedTimer timer;
	bool renderVid = false;
};

#endif // CONTROLPANEL_HPP
