/*
    Copyright (C) 2023 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PREPROC3D_HPP
#define PREPROC3D_HPP

#include <QSettings>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <vector>

/** @brief This is a CPU replacement for the preproc3D.comp shader, as it
 * doesn't run on low-end hardware.
 */
class Preproc3D
{
  public:
	Preproc3D(std::vector<float> const& dataIn,
	          std::vector<float> const& dataIn2, std::vector<float>& dataOut,
	          QVector2D const& texSize, QVector3D const& imSize, float minR,
	          float maxR, bool autograv);

	void computeVolumetricLight();

  private:
	void computeSlice(int startZ, int endZ);
	QVector3D computePoint(int x, int y, int z);

	float getDensity(const QVector4D& pt);
	float getDensityIntegral(const QVector4D& ptA, const QVector4D& ptB);
	QVector3D getDirectStarLight(const QVector4D& pt);

	std::vector<float> const& m_dataIn;
	std::vector<float> const& m_dataIn2;
	std::vector<float>& m_dataOut;

	QVector2D texSize;
	QVector3D imSize;

	float minR;
	float maxR;
	bool autograv;

	const float pshUnit = QSettings().value("data/pshUnit").toString() == "au"
	                          ? 1.f
	                          : 6.68459e-14;
};

#endif // PREPROC3D_HPP
