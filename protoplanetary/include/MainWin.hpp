#ifndef MAINWIN_H
#define MAINWIN_H

#include "AbstractMainWin.hpp"
#include "Disk.hpp"

/** @ingroup pycall
 *
 * @brief Main window to be displayed.
 *
 * Callable in Python as the "Protoplanetary" object.
 */
class MainWin : public AbstractMainWin
{
	Q_OBJECT
  public:
	Q_PROPERTY(float camPitch MEMBER pitch)
	Q_PROPERTY(float camYaw MEMBER yaw)
	Q_PROPERTY(float camZoom MEMBER zoom)
	Q_PROPERTY(float animationTime READ getAnimationTime)

	MainWin() = default;

  public slots:
	void reloadDiskPreproc() { disk->reloadPreproc(); };

  protected:
	virtual void mousePressEvent(QMouseEvent* e) override;
	virtual void mouseReleaseEvent(QMouseEvent* e) override;
	virtual void mouseMoveEvent(QMouseEvent* e) override;
	virtual void wheelEvent(QWheelEvent* e) override;

	virtual void setupPythonAPI() override;

	// declare drawn resources
	virtual void initScene() override;

	// update physics/controls/meshes, etc...
	// prepare for rendering
	virtual void updateScene(BasicCamera& camera,
	                         QString const& pathId) override;

	// render user scene on camera
	// (no controllers or hands)
	virtual void renderScene(BasicCamera const& camera,
	                         QString const& pathId) override;

	virtual void renderGui(QSize const& targetSize,
	                       AdvancedPainter& painter) override;

	float getAnimationTime() const { return disk->getAnimationTime(); }

  private:
	float zoom = 50.f;
	std::unique_ptr<Disk> disk;

	// mouse controls variables
	bool rotateViewEnabled = false;
	bool trackballEnabled  = false;
	float pitch            = M_PI_4;
	float yaw              = 0.f;

	bool loading = true;
	std::unique_ptr<QLabel> tooltip;
};

#endif // MAINWIN_H
