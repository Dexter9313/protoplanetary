##Setup libraries, use find_package etc...

## These variables must be set by the end of this file
#set(PROJECT_INCLUDE_DIRS ${Boost_INCLUDE_DIRS})
#set(PROJECT_LIBRARIES ${Boost_LIBRARIES})

find_package (Threads)

set(PROJECT_LIBRARIES ${PROJECT_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})
