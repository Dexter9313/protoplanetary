/*
    Copyright (C) 2022 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Disk.hpp"

#include <QCoreApplication>
#include <QDir>
#include <QElapsedTimer>
#include <QOpenGLPaintDevice>
#include <QProgressDialog>
#include <QThread>
#include <QtDebug>
#include <cmath>
#include <thread>

#include "Blackbody.hpp"
#include "Preproc3D.hpp"
#include "Primitives.hpp"

Disk::Disk(QString const& dirpath, QString const& planetdirpath)
    : shader("disk")
    , shader3D("disk3D")
    , preproc3D({{"preproc3D", GLShaderProgram::Stage::COMPUTE}})
    , shaderPlanets("planets")
    , shaderPlanets3D("planets3D")
    , shaderScale("scale", "default")
    , gradientDispShader("gradient", "gradient")
    , controlPanel(gradient)
{
	// LOAD PLANETS
	{
		std::thread threadplanet([this, planetdirpath]()
		                         { loadPlanets(planetdirpath); });
		QProgressDialog progressplanet(QObject::tr("Loading planet files..."),
		                               "", 0, timesteps);
		progressplanet.setCancelButton(nullptr);
		progressplanet.show();

		while((timesteps == 0 || timestep < timesteps) && timesteps != INT_MAX)
		{
			progressplanet.setValue(timestep);
			progressplanet.setMaximum(timesteps);
			QCoreApplication::processEvents();
			QThread::msleep(50);
		}
		threadplanet.join();
	}

	meshPlanets.setVertexShaderMapping(
	    shaderPlanets,
	    {{"position", 3}, {"status", 1}, {"position2", 3}, {"status2", 1}});
	meshPlanets.setVertices(planetData);

	// LOAD DISK
	{
		std::thread thread([this, dirpath]() { loadDisk(dirpath); });
		QProgressDialog progress(QObject::tr("Loading disk files..."), "", 0,
		                         timesteps);
		progress.setCancelButton(nullptr);
		progress.show();

		timestep = 0;
		while((timesteps == 0 || timestep < timesteps) && timesteps != INT_MAX)
		{
			progress.setValue(timestep);
			progress.setMaximum(timesteps);
			QCoreApplication::processEvents();
			QThread::msleep(50);
		}
		thread.join();
	}

	shader.setUniform("minR", minR);
	shader.setUniform("maxR", maxR);
	shader.setUniform("pshUnit", pshUnit);
	shader.setUniform("tex", 0);
	shader.setUniform("texIntegral", 1);
	Primitives::setAsQuad(mesh, shader);

	shader3D.setUniform("minR", minR);
	shader3D.setUniform("maxR", maxR);
	shader3D.setUniform("pshUnit", pshUnit);
	shader3D.setUniform("tex", 0);
	shader3D.setUniform("preproc", 1);
	shader3D.setUniform("tex2", 2);
	Primitives::setAsUnitCylinder(mesh3D, shader, 100);

	tex = std::make_unique<GLTexture>(
	    GLTexture::Tex2DProperties(spacesteps, timesteps, GL_RGBA32F),
	    GLTexture::Sampler(GL_LINEAR, GL_CLAMP_TO_BORDER),
	    GLTexture::Data(diskData.data(), GL_FLOAT, GL_RGBA));

	tex2 = std::make_unique<GLTexture>(
	    GLTexture::Tex2DProperties(spacesteps, timesteps, GL_RGBA32F),
	    GLTexture::Sampler(GL_LINEAR, GL_CLAMP_TO_BORDER),
	    GLTexture::Data(diskData2.data(), GL_FLOAT, GL_RGB));

	texIntegral = std::make_unique<GLTexture>(
	    GLTexture::Tex2DProperties(spacesteps, timesteps, GL_R32F),
	    GLTexture::Sampler(GL_LINEAR, GL_CLAMP_TO_BORDER),
	    GLTexture::Data(diskDataDensityIntegral.data(), GL_FLOAT, GL_RED));

	QVector3D texIntegral3DSize(spacesteps / 10.f, spacesteps / 100.f,
	                            timesteps / 100.f);
	texIntegral3D = std::make_unique<GLTexture>(
	    GLTexture::Tex3DProperties(texIntegral3DSize.x(), texIntegral3DSize.y(),
	                               texIntegral3DSize.z(), GL_RGBA32F),
	    GLTexture::Sampler(GL_LINEAR, GL_CLAMP_TO_BORDER),
	    GLTexture::Data(nullptr, GL_FLOAT, GL_RED));

	// compute 3D density integral
	reloadPreproc();

	meshScale.setVertexShaderMapping(shaderScale, {{"xposition", 1}});
	meshScale.setVertices({0.f, 0.25f});

	Primitives::setAsQuad(gradientDispMesh, gradientDispShader);

	controlPanel.setTimesteps(timesteps);
	controlPanel.show();
}

void Disk::reloadPreproc()
{
	QVector3D texIntegral3DSize(spacesteps / 10.f, spacesteps / 100.f,
	                            timesteps / 100.f);
	if(QSettings().value("data/cpupreproc3D").toBool())
	{
		Preproc3D preproc3DCPU(diskData, diskData2, diskDataDensityIntegral3D,
		                       QVector2D(spacesteps, timesteps),
		                       texIntegral3DSize, minR, maxR, autograv);
		preproc3DCPU.computeVolumetricLight();
		texIntegral3D->setData({diskDataDensityIntegral3D.data(), GL_FLOAT});
	}
	else
	{
		std::array<unsigned int, 3> groupSize{};
		groupSize[0] = texIntegral3DSize[0];
		groupSize[1] = texIntegral3DSize[1];
		groupSize[2] = texIntegral3DSize[2];
		preproc3D.setUniform("minR", minR);
		preproc3D.setUniform("maxR", maxR);
		preproc3D.setUniform("pshUnit", pshUnit);
		preproc3D.setUniform("autogravitation", autograv ? 1.f : 0.f);
		preproc3D.setUniform("texSize", QVector2D(spacesteps, timesteps));
		preproc3D.setUniform("imSize", texIntegral3DSize);
		preproc3D.exec({{tex.get(), GLComputeShader::R},
		                {tex2.get(), GLComputeShader::R},
		                {texIntegral3D.get(), GLComputeShader::W}},
		               groupSize);
	}
}

void Disk::render(float zoom, float aspectRatio, ToneMappingModel const& tmm)
{
	GLStateSet glState({{GL_DEPTH_TEST, false}});
	{
		gradient.setShaderUniforms(shader);
		shader.setUniform("time", controlPanel.getCorrectedAnimationTime());
		shader.setUniform("zoom", zoom);
		shader.setUniform("aspectRatio", aspectRatio);
		shader.setUniform("exposure", tmm.exposure);
		shader.setUniform("dynamicrange", tmm.dynamicrange);
		shader.setUniform("ul", static_cast<int>(controlPanel.getVisTypeUL()));
		shader.setUniform("ur", static_cast<int>(controlPanel.getVisTypeUR()));
		shader.setUniform("dl", static_cast<int>(controlPanel.getVisTypeDL()));
		shader.setUniform("dr", static_cast<int>(controlPanel.getVisTypeDR()));
		shader.setUniform("starColor", blackbody::colorFromTemperature(
		                                   controlPanel.getStarTemperature()));
		GLStateSet glState({{GL_CULL_FACE, false}});
		GLHandler::useTextures({tex.get(), texIntegral.get()});
		GLHandler::setUpRender(shader);
		mesh.render(PrimitiveType::TRIANGLE_STRIP);
	}

	GLHandler::setPointSize(5);
	shaderPlanets.setUniform("color", controlPanel.getPlanetsColor());
	shaderPlanets.setUniform("zoom", zoom / maxR);
	shaderPlanets.setUniform("aspectRatio", aspectRatio);
	shaderPlanets.setUniform("exposure", tmm.exposure);
	shaderPlanets.setUniform("dynamicrange", tmm.dynamicrange);
	shaderPlanets.setUniform(
	    "timestep", controlPanel.getCorrectedAnimationTime() * (timesteps - 1));

	GLHandler::setUpRender(shaderPlanets);
	meshPlanets.drawArrays(
	    floor(controlPanel.getCorrectedAnimationTime() * (timesteps - 1))
	        * planets,
	    planets);
	GLHandler::setPointSize(1);

	if(controlPanel.drawScale())
	{
		float oneEighthScreenScale(0.25 * maxR / zoom);
		closestGreatUnit
		    = QString::number(oneEighthScreenScale, 'g', 1).toFloat();
		float factor(closestGreatUnit / oneEighthScreenScale);

		shaderScale.setUniform("start", 0.6f);
		shaderScale.setUniform("scale", factor);

		shaderScale.setUniform("color", controlPanel.getGuiColor());
		shaderScale.setUniform("exposure", tmm.exposure);
		shaderScale.setUniform("dynamicrange", tmm.dynamicrange);
		meshScale.render(PrimitiveType::LINES);
	}

	if(controlPanel.drawColorMap())
	{
		gradient.setShaderUniforms(gradientDispShader);
		gradientDispShader.setUniform("exposure", tmm.exposure);
		gradientDispShader.setUniform("dynamicrange", tmm.dynamicrange);
		gradientDispShader.setUniform("useLog", 0.f);
		GLHandler::setUpRender(gradientDispShader);
		gradientDispMesh.render(PrimitiveType::TRIANGLE_STRIP);
	}
}

void Disk::render3D(QVector3D const& campos, ToneMappingModel const& tmm)
{
	if(autograv != controlPanel.autoGravitation())
	{
		autograv = !autograv;
		reloadPreproc();
	}
	shader3D.setUniform("campos", campos);
	shader3D.setUniform("time", controlPanel.getCorrectedAnimationTime());
	shader3D.setUniform("starColor", blackbody::colorFromTemperature(
	                                     controlPanel.getStarTemperature()));
	shader3D.setUniform("samples",
	                    static_cast<float>(controlPanel.get3DSamples()));
	shader3D.setUniform("autogravitation", autograv ? 1.f : 0.f);
	{
		GLCullFaceSet glCullFaceState(GL_BACK);
		GLHandler::useTextures({tex.get(), texIntegral3D.get(), tex2.get()});
		QMatrix4x4 scale;
		scale.scale({1.f, 1.f, 0.1f});
		GLHandler::setUpRender(shader3D, scale);
		mesh3D.render();
	}

	GLHandler::setPointSize(5);
	shaderPlanets3D.setUniform("color", controlPanel.getPlanetsColor());
	shaderPlanets3D.setUniform("maxR", maxR);
	shaderPlanets3D.setUniform("exposure", tmm.exposure);
	shaderPlanets3D.setUniform("dynamicrange", tmm.dynamicrange);
	shaderPlanets3D.setUniform(
	    "timestep", controlPanel.getCorrectedAnimationTime() * (timesteps - 1));
	GLHandler::setUpRender(shaderPlanets3D);
	meshPlanets.drawArrays(
	    floor(controlPanel.getCorrectedAnimationTime() * (timesteps - 1))
	        * planets,
	    planets);
	GLHandler::setPointSize(1);
}

void Disk::renderGui(QSize const& targetSize)
{
	QOpenGLPaintDevice d(targetSize);
	QPainter painter(&d);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.setRenderHint(QPainter::TextAntialiasing);
	QPen pen(controlPanel.getGuiColor());
	painter.setFont(controlPanel.getGuiFont());
	painter.setPen(pen);
	auto w(targetSize.width()), h(targetSize.height());
	if(controlPanel.drawYear())
	{
		painter.drawText(0, 0, w, h, Qt::AlignHCenter | Qt::AlignTop,
		                 "Year " + QString::number(getTimeYears()));
	}

	if(controlPanel.drawScale() && !controlPanel.is3D())
	{
		painter.drawText(0.825 * w, 0, w, 9 * h / 20,
		                 Qt::AlignLeft | Qt::AlignBottom,
		                 QString::number(closestGreatUnit) + " AU");
	}

	painter.drawText(0, 0, w, h, Qt::AlignHCenter | Qt::AlignBottom,
	                 controlPanel.getCustomText());

	if(controlPanel.drawColorMap() && !controlPanel.is3D())
	{
		QString str = QString::number(gradient.maxVal);
		if(gradient.useLog)
		{
			str = "log(" + str + ")";
		}
		painter.drawText(0.015 * w, 0, w, h * 0.45,
		                 Qt::AlignLeft | Qt::AlignVCenter, str);
		str = QString::number(gradient.minVal);
		if(gradient.useLog)
		{
			str = "log(" + str + ")";
		}
		painter.drawText(0.015 * w, 0.58 * h, w, 0.38 * h,
		                 Qt::AlignLeft | Qt::AlignVCenter, str);
	}

	if(controlPanel.is3D() || !controlPanel.drawGui())
	{
		painter.end();
		return;
	}

	painter.drawText(0, 0, w, h, Qt::AlignLeft | Qt::AlignTop,
	                 controlPanel.getVisTypeStr(controlPanel.getVisTypeUL()));
	painter.drawText(0, 0, w, h, Qt::AlignRight | Qt::AlignTop,
	                 controlPanel.getVisTypeStr(controlPanel.getVisTypeUR()));
	painter.drawText(0, 0, w, h, Qt::AlignLeft | Qt::AlignBottom,
	                 controlPanel.getVisTypeStr(controlPanel.getVisTypeDL()));
	painter.drawText(0, 0, w, h, Qt::AlignRight | Qt::AlignBottom,
	                 controlPanel.getVisTypeStr(controlPanel.getVisTypeDR()));
	painter.end();
}

void Disk::loadPlanets(QString const& dirpath)
{
	QDir dir(dirpath);

	auto allDatFiles = dir.entryList({"*.dat"});
	timesteps        = allDatFiles.size();
	if(timesteps == 0)
	{
		timesteps = INT_MAX;
		qWarning() << "No .dat files in directory " + dirpath;
	}

	auto fileZero = dir.entryList({"*_0.dat"})[0];
	auto prefix   = fileZero.split('_')[0];
	QFile inputFile(dirpath + '/' + fileZero);
	if(inputFile.open(QIODevice::ReadOnly))
	{
		QTextStream in(&inputFile);
		QString line = in.readLine();
		auto fields  = line.split(' ');
		fields.removeAll(QString(""));
		planets = (fields.size() - 1) / 4;
	}

	planetData.reserve(8 * planets * timesteps);

	for(timestep = 0; timestep < timesteps; ++timestep)
	{
		QString fileName(prefix + '_' + QString::number(timestep) + ".dat");
		QFile inputFile(dirpath + '/' + fileName);
		if(inputFile.open(QIODevice::ReadOnly))
		{
			QTextStream in(&inputFile);
			QString line = in.readLine();
			auto fields  = line.split(' ');
			fields.removeAll(QString(""));
			if(fields[0].toFloat() < minTime)
			{
				minTime = fields[0].toFloat();
			}
			if(fields[0].toFloat() > maxTime)
			{
				maxTime = fields[0].toFloat();
			}
			for(unsigned int i(0); i < planets; ++i)
			{
				auto cursor = planetData.size();
				planetData.push_back(fields[1 + i].toFloat());
				planetData.push_back(fields[1 + i + planets].toFloat());
				planetData.push_back(fields[1 + i + 2 * planets].toFloat());
				planetData.push_back(fields[1 + i + 3 * planets].toFloat());
				// reserve for next timestep
				if(timestep < timesteps - 1)
				{
					planetData.push_back(0.f);
					planetData.push_back(0.f);
					planetData.push_back(0.f);
					planetData.push_back(0.f);
				}
				// write for previous timestep
				if(timestep > 0)
				{
					cursor -= 8 * planets;
					cursor += 4;
					planetData[cursor]     = fields[1 + i].toFloat();
					planetData[cursor + 1] = fields[1 + i + planets].toFloat();
					planetData[cursor + 2]
					    = fields[1 + i + 2 * planets].toFloat();
					planetData[cursor + 3]
					    = fields[1 + i + 3 * planets].toFloat();
				}
			}
		}
	}
}

void Disk::loadDisk(QString const& dirpath)
{
	QDir dir(dirpath);

	// read first file to get number of space steps
	auto fileZero = dir.entryList({"*_0.dat"})[0];
	auto prefix   = fileZero.split('_')[0];
	QFile inputFile(dirpath + '/' + fileZero);
	if(inputFile.open(QIODevice::ReadOnly))
	{
		QTextStream in(&inputFile);
		while(!in.atEnd())
		{
			QString line = in.readLine();
			auto fields  = line.split(' ');
			fields.removeAll(QString(""));

			auto R = fields[0].toFloat();
			if(minR > R)
			{
				minR = R;
			}
			if(maxR < R)
			{
				maxR = R;
			}

			auto sig = fields[1].toFloat();
			if(minSig > sig)
			{
				minSig = sig;
			}
			if(maxSig < sig && sig < 1000)
			{
				maxSig = sig;
			}

			auto T = fields[2].toFloat();
			if(minT > T)
			{
				minT = T;
			}
			if(maxT < T && T < 1000)
			{
				maxT = T;
			}

			auto psh = fields[3].toFloat();
			if(minPsh > psh)
			{
				minPsh = psh;
			}
			if(maxPsh < psh)
			{
				maxPsh = psh;
			}

			diskData.push_back(sig);
			diskData.push_back(T);
			diskData.push_back(psh);
			diskData.push_back(fields[4].toFloat());

			diskData2.push_back(fields[5].toFloat());
			diskData2.push_back(fields[6].toFloat());
			diskData2.push_back(fields[7].toFloat());
		}
		inputFile.close();
	}

	spacesteps = diskData.size() / variablesNumber;

	diskData.resize(timesteps * spacesteps * variablesNumber);
	diskData2.reserve(timesteps * spacesteps * 3);
	diskDataDensityIntegral.resize(timesteps * spacesteps);

	for(timestep = 1; timestep < timesteps; ++timestep)
	{
		QString fileName(prefix + '_' + QString::number(timestep) + ".dat");
		QFile inputFile(dirpath + '/' + fileName);
		if(inputFile.open(QIODevice::ReadOnly))
		{
			QTextStream in(&inputFile);
			unsigned int spacestep = 0;
			float oldDist          = 0.f;
			while(!in.atEnd())
			{
				QString line = in.readLine();
				auto fields  = line.split(' ');
				fields.removeAll(QString(""));

				auto sig = fields[1].toFloat();
				if(minSig > sig)
				{
					minSig = sig;
				}
				if(maxSig < sig && sig < 1000)
				{
					maxSig = sig;
				}

				auto T = fields[2].toFloat();
				if(minT > T)
				{
					minT = T;
				}
				if(maxT < T && T < 1000)
				{
					maxT = T;
				}

				auto psh = fields[3].toFloat();
				if(minPsh > psh)
				{
					minPsh = psh;
				}
				if(maxPsh < psh)
				{
					maxPsh = psh;
				}

				setData(timestep, spacestep, Variable::SIGMA, sig);
				setData(timestep, spacestep, Variable::TEMPE, T);
				setData(timestep, spacestep, Variable::P_S_H, psh);
				setData(timestep, spacestep, Variable::QTOOM,
				        fields[4].toFloat());
				diskData2.push_back(fields[5].toFloat());
				diskData2.push_back(fields[6].toFloat());
				diskData2.push_back(fields[7].toFloat());

				auto dist(fields[0].toFloat());
				diskDataDensityIntegral[(timestep * spacesteps) + spacestep]
				    = (dist - oldDist) * sig;
				oldDist = dist;
				if(spacestep > 0)
				{
					diskDataDensityIntegral[(timestep * spacesteps) + spacestep]
					    += diskDataDensityIntegral[(timestep * spacesteps)
					                               + spacestep - 1];
				}

				++spacestep;
			}
			inputFile.close();
		}
	}
}

float Disk::getData(unsigned int timestep, unsigned int spacestep,
                    Variable var) const
{
	if(timestep >= timesteps || spacestep >= spacesteps)
	{
		return 0;
	}
	return diskData[(timestep * spacesteps * variablesNumber)
	                + (spacestep * variablesNumber)
	                + static_cast<unsigned int>(var)];
}

float Disk::getData(float relTime, float relScreenSpace, float zoom,
                    Variable var)
{
	float R = relScreenSpace * maxR;
	R /= zoom;

	if(R > maxR)
	{
		return 0.f;
	}

	float logR    = log(R);
	float minLogR = log(minR);
	float maxLogR = log(maxR);

	float relSpace = (logR - minLogR) / (maxLogR - minLogR);
	return getData(relTime * (timesteps - 1), relSpace * (spacesteps - 1), var);
}

void Disk::setData(unsigned int timestep, unsigned int spacestep, Variable var,
                   float val)
{
	diskData[(timestep * spacesteps * variablesNumber)
	         + (spacestep * variablesNumber) + static_cast<unsigned int>(var)]
	    = val;
}
