#include "MainWin.hpp"

void MainWin::mousePressEvent(QMouseEvent* e)
{
	if(e->button() == Qt::MouseButton::LeftButton)
	{
		rotateViewEnabled = true;
		QCursor::setPos(x() + width() / 2, y() + height() / 2);
	}
	else if(e->button() == Qt::MouseButton::RightButton)
	{
		trackballEnabled = true;
		QCursor::setPos(x() + width() / 2, y() + height() / 2);
	}
}

void MainWin::mouseReleaseEvent(QMouseEvent* e)
{
	if(e->button() == Qt::MouseButton::LeftButton)
	{
		rotateViewEnabled = false;
	}
	else if(e->button() == Qt::MouseButton::RightButton)
	{
		trackballEnabled = false;
	}
}

void MainWin::mouseMoveEvent(QMouseEvent* e)
{
	// update label
	if(!loading && disk->getControlPanel().drawMouseHoverTooltip()
	   && !disk->getControlPanel().is3D())
	{
		QPointF relPos = e->pos() - QPointF(width() * 0.5, height() * 0.5);
		auto c = sqrt(relPos.x() * relPos.x() + relPos.y() * relPos.y()) * 2.0
		         / width();
		auto t = disk->getCorrectedAnimationTime();

		QString str;
		str += "R: " + QString::number(c * disk->getMaxR() / zoom) + "\n";
		str += "Density: "
		       + QString::number(
		           disk->getData(t, c, zoom, Disk::Variable::SIGMA))
		       + "\n";
		str += "Temperature: "
		       + QString::number(
		           disk->getData(t, c, zoom, Disk::Variable::TEMPE))
		       + "\n";
		str += "Pressure scale height: "
		       + QString::number(
		           disk->getData(t, c, zoom, Disk::Variable::P_S_H))
		       + "\n";
		str += "Toomre Q param. : "
		       + QString::number(
		           disk->getData(t, c, zoom, Disk::Variable::QTOOM));
		tooltip->setText(str);
		tooltip->show();

		auto p = geometry().topLeft() + e->pos() + QPoint(20, 0);
		auto s = tooltip->size();
		tooltip->setGeometry(p.x(), p.y(), s.width(), s.height());
	}
	else
	{
		tooltip->hide();
	}

	// update 3D view
	if(!rotateViewEnabled && !trackballEnabled)
	{
		return;
	}

	float dx = (x() + static_cast<float>(width()) / 2 - e->globalX()) / width();
	float dy
	    = (y() + static_cast<float>(height()) / 2 - e->globalY()) / height();

	if(rotateViewEnabled || trackballEnabled)
	{
		double dYaw(dx * M_PI / 3.0), dPitch(dy * M_PI / 3.0);

		if(pitch + dPitch > M_PI_2 - 0.20)
		{
			dPitch = M_PI_2 - 0.20 - pitch;
		}
		if(pitch + dPitch < -1.f * M_PI_2 + 0.2)
		{
			dPitch = -1.f * M_PI_2 + 0.2 - pitch;
		}
		yaw += dYaw;
		pitch += dPitch;
	}
	QCursor::setPos(x() + width() / 2, y() + height() / 2);
}

void MainWin::wheelEvent(QWheelEvent* e)
{
	zoom *= (1.f + e->angleDelta().y() / 1000.f);
}

void MainWin::setupPythonAPI()
{
	PythonQtHandler::addObject("Protoplanetary", this);
}

void MainWin::initScene()
{
	tooltip = std::make_unique<QLabel>("");
	tooltip->setAttribute(Qt::WA_TransparentForMouseEvents);
	tooltip->setWindowFlags(tooltip->windowFlags() | Qt::FramelessWindowHint
	                        | Qt::WindowStaysOnTopHint);
	disk = std::make_unique<Disk>(
	    QSettings().value("data/diskdir").toString(),
	    QSettings().value("data/planetdir").toString());

	toneMappingModel->dynamicrange = 10000.f;
	toneMappingModel->exposure     = 0.3f;

	loading = false;
}

void MainWin::updateScene(BasicCamera& camera, QString const& /*pathId*/)
{
	videomode = disk->getControlPanel().updateTimer(videomode);
	float x   = cos(yaw) * cos(pitch);
	float y   = sin(yaw) * cos(pitch);
	float z   = sin(pitch);
	QVector3D pos(x, y, z);

	camera.lookAt(pos * 150.f / zoom, {}, {0.f, 0.f, 1.f});
}

void MainWin::renderScene(BasicCamera const& camera, QString const& /*pathId*/)
{
	if(disk->getControlPanel().is3D())
	{
		disk->render3D(camera.getView().inverted().column(3).toVector3D(),
		               *toneMappingModel);
	}
	else
	{
		disk->render(zoom,
		             static_cast<float>(renderer.getSize().width())
		                 / renderer.getSize().height(),
		             *toneMappingModel);
	}
}

void MainWin::renderGui(QSize const& targetSize, AdvancedPainter& /*painter*/)
{
	disk->renderGui(targetSize);
}
