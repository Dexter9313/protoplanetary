/*
    Copyright (C) 2023 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "ControlPanel.hpp"

#include <QCoreApplication>
#include <QFormLayout>
#include <QLabel>
#include <QSettings>

#include "Blackbody.hpp"

ControlPanel::ControlPanel(grd::Gradient& gradient)
    : animTimeSlider(Qt::Horizontal, this)
    , playPauseBtn("PLAY", this)
    , renderVidBtn("RENDER VIDEO", this)
    , guiColor(tr("GUI color"), this)
    , guiFont(tr("GUI font"), this)
    , planetsColor(tr("Planets color"), this)
    , selector(gradient)
    , visTypesLabels({tr("Physics-Based Rendering"), tr("Density (g/cm^2)"),
                      tr("Temperature (K)"), tr("Pressure Scale Height (AU)"),
                      tr("Toomre Criterion")})
{
	setWindowTitle(tr("Control Panel"));

	auto mainlayout = make_qt_unique<QHBoxLayout>(*this);

	animTimeSlider.setRange(0, 1000);
	connect(&animTimeSlider, &QSlider::valueChanged,
	        [this](int v)
	        {
		        this->pausedAt = v / 1000.0;
		        if(timer.isValid())
		        {
			        timer.restart();
		        }
	        });
	minTimestep.setRange(0, 10000);
	minTimestep.setValue(0);
	maxTimestep.setRange(0, 10000);
	maxTimestep.setValue(10000);
	animDuration.setRange(1, 10000);
	animDuration.setValue(165);
	connect(&playPauseBtn, &QPushButton::pressed,
	        [this]()
	        {
		        if(timer.isValid())
		        {
			        float time
			            = pausedAt
			              + timer.elapsed() / 1000.0 / animDuration.value();
			        pausedAt = time;
			        playPauseBtn.setText("PLAY");
			        timer.invalidate();
		        }
		        else
		        {
			        playPauseBtn.setText("PAUSE");
			        timer.start();
		        }
	        });
	connect(&renderVidBtn, &QPushButton::pressed,
	        [this]()
	        {
		        renderVid = true;
		        pausedAt  = 0.f;
		        timer.invalidate();
	        });

	quitWhenRendered.setChecked(false);
	is3DCbox.setChecked(false);
	autogravCbox.setChecked(false);
	samples.setRange(10, 2000);
	samples.setValue(50);
	mouseHoverTooltip.setChecked(false);
	for(auto const& s : visTypesLabels)
	{
		ulCbox.addItem(s);
		urCbox.addItem(s);
		dlCbox.addItem(s);
		drCbox.addItem(s);
	}
	urCbox.setCurrentIndex(1);
	dlCbox.setCurrentIndex(2);
	drCbox.setCurrentIndex(3);
	drawYearCbox.setChecked(true);
	drawScaleCbox.setChecked(true);
	drawColorMapCbox.setChecked(true);
	drawGuiCbox.setChecked(true);
	guiColor.setColor(QColor(255, 0, 0));
	QFont font;
	font.setPointSizeF(font.pointSize() * 2);
	guiFont.setFont(font);
	starTemp.setRange(blackbody::min_temp, blackbody::max_temp);
	starTemp.setValue(5772);
	planetsColor.setColor(QColor(0, 255, 255));
	auto w = make_qt_unique<QWidget>(*this);
	mainlayout->addWidget(w);
	auto layout = make_qt_unique<QFormLayout>(*w);

	layout->addRow(tr("Animation time : "), &animTimeSlider);
	layout->addRow(tr("Minimum timestep : "), &minTimestep);
	layout->addRow(tr("Maximum timestep : "), &maxTimestep);
	layout->addRow(tr("Animation duration (s) : "), &animDuration);
	layout->addRow(&playPauseBtn);
	layout->addRow(&renderVidBtn);
	layout->addRow(tr("Quit when rendered : "), &quitWhenRendered);
	layout->addRow(tr("3D : "), &is3DCbox);
	layout->addRow(tr("Autogravitation : "), &autogravCbox);
	layout->addRow(tr("3D raymarching samples : "), &samples);
	layout->addRow(tr("Mouse Hover Tooltip : "), &mouseHoverTooltip);
	layout->addRow(tr("Up-Left quadrant : "), &ulCbox);
	layout->addRow(tr("Up-Right quadrant : "), &urCbox);
	layout->addRow(tr("Down-Left quadrant : "), &dlCbox);
	layout->addRow(tr("Down-Right quadrant : "), &drCbox);
	layout->addRow(tr("Draw Year : "), &drawYearCbox);
	layout->addRow(tr("Draw Scale : "), &drawScaleCbox);
	layout->addRow(tr("Draw Color Map : "), &drawColorMapCbox);
	layout->addRow(tr("Draw GUI : "), &drawGuiCbox);
	layout->addRow(tr("GUI Color : "), &guiColor);
	layout->addRow(tr("GUI Font : "), &guiFont);
	layout->addRow(tr("Star Temperature (K) : "), &starTemp);
	layout->addRow(tr("Planets Color : "), &planetsColor);
	layout->addRow(tr("Custom text : "), &customText);

	w = make_qt_unique<QWidget>(*this);
	mainlayout->addWidget(w);
	layout = make_qt_unique<QFormLayout>(*w);
	layout->addRow(make_qt_unique<QLabel>(*this, tr("<u><b>Gradient</b></u>")));
	layout->addRow(&selector);
}

float ControlPanel::getAnimationTime()
{
	if(timer.isValid())
	{
		float time = pausedAt + timer.elapsed() / 1000.0 / animDuration.value();
		if(time > 1.f)
		{
			playPauseBtn.setText("PLAY");
			pausedAt = 0.f;
			time     = 0.f;
			timer.invalidate();
		}
		animTimeSlider.blockSignals(true);
		animTimeSlider.setValue(time * 1000.0);
		animTimeSlider.blockSignals(false);
		return time;
	}
	animTimeSlider.blockSignals(true);
	animTimeSlider.setValue(pausedAt * 1000.0);
	animTimeSlider.blockSignals(false);
	return pausedAt;
}

float ControlPanel::getCorrectedAnimationTime()
{
	auto animTime = getAnimationTime();
	auto min = static_cast<float>(minTimestep.value()) / minTimestep.maximum();
	auto max = static_cast<float>(maxTimestep.value()) / maxTimestep.maximum();

	return min + animTime * (max - min);
}

ControlPanel::VisType ControlPanel::getVisTypeUL() const
{
	return static_cast<VisType>(ulCbox.currentIndex());
}

ControlPanel::VisType ControlPanel::getVisTypeUR() const
{
	return static_cast<VisType>(urCbox.currentIndex());
}

ControlPanel::VisType ControlPanel::getVisTypeDL() const
{
	return static_cast<VisType>(dlCbox.currentIndex());
}

ControlPanel::VisType ControlPanel::getVisTypeDR() const
{
	return static_cast<VisType>(drCbox.currentIndex());
}

QString ControlPanel::getVisTypeStr(VisType visType) const
{
	return visTypesLabels.at(static_cast<int>(visType));
}

bool ControlPanel::updateTimer(bool videoModeOn)
{
	if(videoModeOn)
	{
		pausedAt += 1.0 / QSettings().value("window/videofps").toInt()
		            / animDuration.value();
		timer.invalidate();

		bool continueVid = pausedAt < 1.0;
		if(!continueVid)
		{
			pausedAt = 0.f;
			timer.invalidate();
			playPauseBtn.setText("PLAY");
			animTimeSlider.blockSignals(true);
			animTimeSlider.setValue(0);
			animTimeSlider.blockSignals(false);
			if(quitWhenRendered.isChecked())
			{
				QCoreApplication::quit();
			}
		}
		return continueVid;
	}
	if(renderVid)
	{
		renderVid = false;
		return true;
	}
	return false;
}
