
#include "Preproc3D.hpp"

#include <QtConcurrent>
#include <cmath>

Preproc3D::Preproc3D(std::vector<float> const& dataIn,
                     std::vector<float> const& dataIn2,
                     std::vector<float>& dataOut, QVector2D const& texSize,
                     QVector3D const& imSize, float minR, float maxR,
                     bool autograv)
    : m_dataIn(dataIn)
    , m_dataIn2(dataIn2)
    , m_dataOut(dataOut)
    , texSize(texSize)
    , imSize(imSize)
    , minR(minR)
    , maxR(maxR)
    , autograv(autograv)
{
	m_dataOut.resize(static_cast<int>(imSize.x()) * static_cast<int>(imSize.y())
	                 * static_cast<int>(imSize.z()) * 4);
}

void Preproc3D::computeSlice(int startZ, int endZ)
{
	for(int x = 0; x < static_cast<int>(imSize.x()); ++x)
	{
		for(int y = 0; y < static_cast<int>(imSize.y()); ++y)
		{
			for(int z = startZ; z < endZ; ++z)
			{
				QVector3D result = computePoint(x, y, z);

				int index = z * static_cast<int>(imSize.y())
				                * static_cast<int>(imSize.x())
				            + y * static_cast<int>(imSize.x()) + x;
				m_dataOut[index * 4]     = result.x();
				m_dataOut[index * 4 + 1] = result.y();
				m_dataOut[index * 4 + 2] = result.z();
			}
		}
	}
}

QVector3D Preproc3D::computePoint(int x, int y, int z)
{
	float normalizedX = static_cast<float>(x) / imSize.x();
	float normalizedZ = static_cast<float>(y) / imSize.y();
	normalizedZ       = 2.0 * normalizedZ - 1.0;
	normalizedZ /= 10.0f;
	float normalizedT = static_cast<float>(z) / imSize.z();

	QVector4D pt(normalizedX, 0.0f, normalizedZ, normalizedT);
	return getDirectStarLight(pt);
}

float Preproc3D::getDensity(const QVector4D& pt)
{
	QVector2D c = pt.toVector2D() / 30.0;
	float R     = c.length() * maxR;
	if(R < minR || R > maxR)
	{
		return 0.0;
	}

	float logR    = std::log(R);
	float minLogR = std::log(minR);
	float maxLogR = std::log(maxR);

	QVector2D coord;
	coord.setX((logR - minLogR) / (maxLogR - minLogR));
	coord.setY(pt.w());

	int index = static_cast<int>(coord.y() * texSize.y())
	                * static_cast<int>(texSize.x())
	            + static_cast<int>(coord.x() * texSize.x());
	float density     = m_dataIn[index * 4];      // red
	float scaleHeight = m_dataIn[index * 4 + 2];  // blue
	float H0          = m_dataIn2[index * 3 + 1]; // green
	float H1          = m_dataIn2[index * 3 + 2]; // blue
	if(scaleHeight == 0.f)                        // prevent div by 0
	{
		scaleHeight = 1e-7;
	}
	if(H0 == 0.0)
	{
		H0 = 1.0e-7;
	}
	if(H1 == 0.0)
	{
		H1 = 1.0e-7;
	}
	scaleHeight *= pshUnit;
	scaleHeight *= 30.0 / maxR; // from AU to cylinder coordinates
	H0 *= pshUnit;
	H0 *= 30.0 / maxR; // from AU to cylinder coordinates
	H1 *= pshUnit;
	H1 *= 30.0 / maxR; // from AU to cylinder coordinates
	if(!autograv)
	{
		return density * exp(-0.5 * std::pow(abs(pt.z()) / scaleHeight, 2));
	}
	return density * exp(-(abs(pt.z()) / H0 + pow(pt.z() / H1, 2.0)));
}

float Preproc3D::getDensityIntegral(const QVector4D& ptA, const QVector4D& ptB)
{
	const int SAMPLES = 1000;
	QVector4D ds      = (ptB - ptA) / static_cast<float>(SAMPLES);
	QVector4D v       = ptA + ds * 0.5f;

	float integral = 0.0;
	for(int i = 0; i < SAMPLES; i++)
	{
		integral += getDensity(v);
		v += ds;
	}
	return integral * ds.length();
}

QVector3D Preproc3D::getDirectStarLight(const QVector4D& pt)
{
	QVector2D c = pt.toVector2D() / 30.0;
	float R     = c.length() * maxR;
	if(R == 0.0) // prevent div by 0
	{
		R = 1.0e-7;
	}

	float density = getDensity(pt);
	QVector3D integral
	    = QVector3D(4.3, 5.3, 8.5)
	      * getDensityIntegral(QVector4D(0.0, 0.0, 0.0, pt.w()), pt);

	QVector3D result(pow(10.0, -1.0 * integral[0] / 2.5),
	                 pow(10.0, -1.0 * integral[1] / 2.5),
	                 pow(10.0, -1.0 * integral[2] / 2.5));
	result *= density / (R * R);
	return result;
}

void Preproc3D::computeVolumetricLight()
{
	int numSlices = QThread::idealThreadCount();
	std::vector<QFuture<void>> futures;

	int sliceSize = static_cast<int>(imSize.z()) / numSlices;
	for(int i = 0; i < numSlices; ++i)
	{
		int startZ = i * sliceSize;
		int endZ   = (i == numSlices - 1) ? static_cast<int>(imSize.z())
		                                  : startZ + sliceSize;

		futures.push_back(QtConcurrent::run([this, startZ, endZ]()
		                                    { computeSlice(startZ, endZ); }));
	}

	for(auto& future : futures)
	{
		future.waitForFinished();
	}
}
