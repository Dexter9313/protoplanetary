#!/bin/bash

mkdir build ; cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DWERROR=true
export VERSION=$(cat PROJECT_VERSION)
make -j $(nproc)
make package
./tests

