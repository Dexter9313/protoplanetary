#include "MainWin.hpp"

void MainWin::initScene() {}

void MainWin::updateScene(BasicCamera& /*camera*/, QString const& /*pathId*/) {}

void MainWin::renderScene(BasicCamera const& /*camera*/,
                          QString const& /*pathId*/)
{
}
