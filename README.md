# Protoplanetary

This project is about visualization of protoplanetary discs simulations. Its input is radial and 1-dimensional. It can render 2D data in an abstract way but can also attempt to do a realistic rendering of the data, either in 2D or 3D.

## Input

In the launcher, all the parameters related to input are in the "Data" tab. All the other parameters belong to the [HydrogenVR](https://gitlab.com/Dexter9313/hydrogenvr) engine.

### Disk dir 

The disk dir parameter takes a directory containing files of space-separated values. The files should be numbered and match the \*_${number}.dat glob. ${number} being 0, 1, 2, ... (no leading zeros).

It should contain eight columns in this order :

- Radial position (AU)
- 2D Density (g/cm^2)
- Temperature (K)
- H Pressure scale height (AU or cm)
- Toomre criterion
- 3D Density (g.cm^3)
- H0 term for autogravitational scale height (AU or cm)
- H1 term for autogravitational scale height (AU or cm)

The radial position should be shared across all files and follow an exponential progression, only the first and last values and read and regular exponential progression is assumed.

H, H0 and H1 must share the same unit (AU or cm).

### Planet dir
The disk dir parameter takes a directory containing files of space-separated values. The files should be numbered and match the \*_${number}.dat glob. ${number} being 0, 1, 2, ... (no leading zeros).

Each planet is parametrized by its x, y and z positions (in AU) and an integer status variable (a planet is considered renderable if and only if its status is set to 0, and not rendered if status is different from 0).

The file content should contain only one line starting by a timestamp in years, then the list of all x values of the planets, then all their y values, z values, then status. x, y, z and status should of course be provide all in the same order. As for the radial position, only the first and last timestamps are actually read and all timestamps are assumed to follow a linear evolution.

### Use CPU to preprocess volumetric rendering

The implementation of the preprocessing of the volumetric rendering is broken on the mesa driver on Linux (which means Intel integrated graphics or mesa software rendering are broken by default). You can bypass this bug by preprocessing the data on the CPU instead. This doesn't affect the framerate while the program is running, only the loading time at the beginning.

### Pressure scale height unit

Specify the unit of the H, H0, and H1 values.


## Usage

### Animation Controls

In the control panel, the animation time sliders changes the current timestep linearly from the minimum timestep specified to the maximum timestep specified. When the slider is all the way to the left, the "animation time" is considered to be 0, and 1 when all the way to the right. (It is important for understanding the Python API.) This means the current timestep is min_timestep + (max_timestep - min_timestep) * animation_time .

At any animation time you can play or pause the animation for it to advance automatically. The animation time will take "animation duration" seconds to go from 0 to 1.

### Video rendering 

While you can render an animation in real-time, you can also render a video out of it. Before doing so, in the launcher of the program under the "Window" tab, you should set a forced rendering resolution (that will be the video resolution), set a video target FPS, and a output frames directory.

Then, you can launch the program and click "Render" in the control panel. Optionally, you can check the "quit when rendered" box to quit the program when rendering is done.

You will be able to find the generated frames in the launcher-specified directory mentioned earlier. If you don't know what to do with them and/or don't have specific requirements about your video formats, and you are on a Linux system with ffmpeg installed : you can just run the generate_vids.sh script that is automatically put in the frames directory. It will convert any directory containing frames into MP4 videos in an "output" directory. You can of course use this script as a base for your own.

### Scripting

If you want to edit the animation properties (for example, as many things can be controlled using scripts), you can create Python scripts. A demo script (in protoplanetary/data/scripts/) is available and can be activated from the "Scripting" tab of the launcher. To add your own script, create a directory with a main.py file in it. You can declare the initScene() and updateScene() functions in it that will be called by the engine respectively at launch and at every frame. If you put your directory along the "demo" directory, it will get selectable in the drop-down menu. If not, you can still set the directory custom root path (you should still set it as custom root path anyway if you plan to edit the script while the program is running).

The documentation for the Python API is split into several parts.

First, the most important part is this Protoplanetary documentation page which lists all the callable objects in the project : https://dexter9313.gitlab.io/protoplanetary/group__pycall.html . The MainWin, AbstractMainWin and ToneMappingModel classes should be the most useful.

Also, there is the whole [PyQt project](https://wiki.python.org/moin/PyQt). The imports are just a bit different and should be prefixed by PythonQt, ex : `from PythonQt.QtCore import Qt, QDateTime, QDate, QTime, QTimeZone`.

Settable in the Controls tab of the launcher, two shortcuts are interesting for scripting : first the F6 key that reloads the Python script from the file (if set correctly as custom), which is useful for editing without relaunching all the time. Second, the F8 key pops a Python console up which can interpret any valid Python from the engine.

### 2D Mode

The 2D display is subdivided into 4 quadrants, which each can be chosen independently from the control panel. You can combine several of the same mode to cover more than one quadrant, for example to display a physics-based rendering.
You can independently toggle several displayed information : 

- The current year the animation is at.
- A distance scale.
- The gradient colormap (applicable to all quadrants at once - their respective displayed units being the units on the colormap). If the gradient is in log mode, the "log" will be mentioned on the labels.
- A custom text that appears at the bottom of the screen : for credits or additional information.

You can toggle all the displayed information at once using the "Draw GUI" checkbox. Also, you can change their color and font from the control panel.

The planets trajectories are projected in 2D by ignoring their z-components and are linearly interpolated through the timesteps. The color of the planets can be changed from the control panel. 

The physics based rendering is an "artistic simulation" in the sense that the material properties of the disc are unknown, but the light properties through the disc should be correct given what we know about the conditions, which are the density profile of the disc and the color of the star (through its temperature, settable in the control panel). Integration is done using the raymarching technique in which a ray is cast along a given direction starting and ending at specific point and small constant steps are made along the ray integrating the wanted value. For our 2D "volumetric" rendering, absorption is integrated from the star to a point of the disc, then the light (absorbed) is multiplied by the density of the given point (the more matter there is the more light can be diffused to the camera direction - which in 2D is up).

### Gradient 

The gradient tool allows you to modify the color gradient used to represent the abstract 2D data. You can use linear interpolations between a start and end color in different color spaces, or use a bezier interpolation in the CIELab space. Technically, only the (a,b) components are picked using the bezier curve. The L component evolves linearly.

For this last mode, adjusting the first slider at the top only gives indications and doesn't change the gradient itself. It shows for various values of the "L" component how the ab space looks. The minimum and maximum values are controlled using the "minimum luminosity" and "maximum luminosity" sliders.

To adjust the bezier curve, you can move the S (start) and E (end) colors. Keep in mind S is only relevant for the minimum luminosity and E is only relevant for the maximum luminosity, so pick them with the top slider adjusted accordingly.
The C1 and C2 control points define the initial directions for the curve. When adjusting the top slider, you will see a cross moving along the curve. This marks the exact color picked for this value of L. Make sure the cross is always within the valid boundaries of (a,b), which change over L. Adjust your curve until this is the case and that the top display of the gradient doesn't show a some bands of noisy colors, which indicate an invalid color.

Finally below the gradient picker itself, you can define the linear behavior of the gradient, by picking the minimum and maximum values for the gradient, and if it should behave linearly or logarithmically. You can also render the gradient as gray scale using the checkbox (and notice by this way how well the CIELAB color space behaves in terms of luminosity compared to the others).

### 3D Mode 

The 3D mode is, as the 2D physics-based rendering, an "artistic simulation". It depends on the density profile of the disc (with or without autograviation) and the star temperature only and is not representative of the chemistry of the disc for example.
The principle is a bit more complex than in 2D as there is a double integral : first, absorption from the star to any point of the disc (axisymmetry gives out a 2D profile for this absorption term, but it is similar to the 2D case), then the whole ray of interest (from the camera to infinity) that traverses the volume that sums all the light attaining the ray given absorption and a phase function. (The phase function represents the fact that light can be reflected off the disc material more or less depending on the angle of the viewer and the light source. So, the phase function is a coefficient for how much light should be accumulated depending on the angle between the camera view angle and each ray-point's direction to the star.)

If the 3D mode doesn't render any disc, try to check the box to use the CPU to preprocess volumetric rendering, as mentioned above.
As for the 2D mode, you can zoom in and out with the mouse wheel. Additionally you can control the camera's position by holding the mouse left button. You can also trigger autogravitation on and off from the control panel (which will trigger a preprocess).
You can adjust the number of raymarching samples through the volume in the control panel. This represents the tradeoff between quality of the visuals and performance (you can use a low value for setting things up for example, then increase it to the max for video rendering). The value by default is quite low, usually a value of 500 starts giving acceptable visual results.

**Important note :** for performance/quality tradeoff reasons, the 3D version does NOT show the whole disc. There are usually too many details at the center of the disc for a raymarching to capture all those details at the scale of the whole disc. That's why the extent of the shown disc is limited to 1 / 30 of the whole radius.Similarly, a disc height limit had to be decided and the higher the limit the more pixels have to be drawn vertically. An arbitrary limit of an height of abs(z) == whole_radius / 300 was set.

As in 2D, planets positions are interpolated linearly. The only drawable GUI element in 3D mode is the current year.
